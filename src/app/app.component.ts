import { Component,OnInit } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit{
  
  isRed:Boolean;
  isYellow:Boolean;
  isBlue:Boolean;
  randomcolor :String;
  
  constructor () {
      console.log("constructor");
  }

  //methods
  ngOnInit() {
    console.log("the page is ready");
    this.isBlue=true;
    this.isYellow=false;
    this.isRed=false;
  }
  onClickBlue() {   
    this.isBlue=true;
    this.isRed=false;
    this.isYellow=false;
  }

  onClickRed() {   
    this.isBlue=false;
    this.isRed=true;
    this.isYellow=false;
  }

  onClickYellow() {   
    this.isBlue=false;
    this.isRed=false;
    this.isYellow=true;
  }

}
